import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import inbound from '@/components/inbound'
import outbound from '@/components/outbound'
import inventory from '@/components/inventory'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/inbound',
      name: 'inbound',
      component: inbound
    },
    {
      path: '/outbound',
      name: 'outbound',
      component: outbound
    },
    {
      path: '/inventory',
      name: 'inventory',
      component: inventory
    }
  ]
})
