var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '123456',
  database : 'test'
});
 
connection.connect();
 
connection.query('SELECT col1 from test1', function (error, results, fields) {
  if (error) throw error;
  console.log('The solution is: ', results[1].col1);
});
